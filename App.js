import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import PagesNavigator from './navigation/PageNavigator';
import { WareHouseProvider } from './provider/WareHouseProvider';

export default function App() {
  return (
    <WareHouseProvider>
      <PagesNavigator />
    </WareHouseProvider>
  );
}

