import React, {useContext} from 'react';
import { View, Text, Button, StyleSheet, FlatList,SafeAreaView } from 'react-native';
import RECENT from '../../data/recent-dummy-data';
import RecentItem from '../../components/recent/RecentItem';
import CATEGORIES from '../../data/categories-dummy-data';
import CategoryItem from '../../components/categories/CategoryItem';
import Colors from '../../constants/Colors';
import HeaderBg from '../../components/HeaderBg';
import { WareHouseContext } from '../../provider/WareHouseProvider';


const CategoriesScreen = props => {

    const warehouse = useContext(WareHouseContext);
    return (
        <SafeAreaView style={styles.screen}>

            <HeaderBg warehouse = {warehouse.name}>
                <View>
                    <Text style={styles.text}>RECENTLY</Text>
                    <View style={styles.listContainer}>
                        <FlatList
                            horizontal={true}
                            data={RECENT}
                            keyExtractor={item => item.id}
                            renderItem={itemData =>

                                <RecentItem
                                    imageColor={itemData.item.bgColor}
                                    image={itemData.item.image}
                                />
                            }
                        />
                    </View>
                </View>
            </HeaderBg>

            <Text style={styles.text}>CATEGORY</Text>
            <View style={styles.catlistContainer}>
                <FlatList
                    numColumns={3}
                    data={CATEGORIES}
                    keyExtractor={item => item.id}
                    renderItem={itemData =>

                        <CategoryItem
                            imageColor={itemData.item.bgColor}
                            image={itemData.item.image}
                        />
                    }
                />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor:'white'
    },
    
    headerScreen:{
        height:undefined,
        backgroundColor: Colors.primary,
        paddingVertical:10
    },
    headerContentScreen:{
        alignItems: 'center',
        justifyContent: 'center',
    },
    listContainer: {
        
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    catlistContainer: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text:{
        paddingVertical: 5,
        paddingHorizontal: 10
    }
})

CategoriesScreen.navigationOptions = {
    headerShown:false
};

export default CategoriesScreen;