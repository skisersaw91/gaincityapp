import React , { useState, useEffect, useContext } from 'react';
import { View, Text, Button, StyleSheet , Dimensions, Image, SafeAreaView} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import HeaderBg from '../../components/HeaderBg';
import { WareHouseContext } from '../../provider/WareHouseProvider';

const ScanQrCodeScreen = props => {

    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const warehouse = useContext(WareHouseContext);

    useEffect(() => {
        (async () => {
          const { status } = await BarCodeScanner.requestPermissionsAsync();
          setHasPermission(status === 'granted');
        })();
      }, []);

      const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        alert(`Bar code with type ${type} and data ${data} has been scanned!`);
      };
    
      if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
      }
      if (hasPermission === false) {
        return <Text>No access to camera</Text>;
      }

    return (
        
        <SafeAreaView style={styles.screen}>
            <HeaderBg warehouse = {warehouse.name} />

            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={[styles.barCodeContainer]}>

                <Image
                    style={styles.qr}
                    source={require('../../assets/qr_code_frame.png')}
                />
                {scanned && <Text style={styles.cancel} onPress={() => setScanned(false)}>Tap to Scan Again</Text>}

            </BarCodeScanner>

        </SafeAreaView>
       
    );
};

const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor:'white'
    },
    barCodeContainer: {
        height: '100%',
        width: '100%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    qr: {
        height: 250,
        width: 250,
    },
    cancel: {
        fontSize: width * 0.05,
        textAlign: 'center',
        width: '70%',
        color: 'white',
    }
})

ScanQrCodeScreen.navigationOptions = {
    headerShown:false
};

export default ScanQrCodeScreen;