import React, { useState, useContext, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import Input from '../../components/Input';
import WareHouseItem from '../../components/warehouse/WarehouseItem';
import WAREHOUSE from '../../data/warehouse-dummy-data';
import HeaderBg from '../../components/HeaderBg';
import { WareHouseContext } from '../../provider/WareHouseProvider';

const SelectLocationScreen = props => {

    const staffId = props.navigation.getParam('staffId');
    const staffName = props.navigation.getParam('staffName');
    const [currentDate, setCurrentDate] = useState('');
    useEffect(() => {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear(); //Current Year
        setCurrentDate(date + '/' + month + '/' + year);
    }, []);


    const warehouse = useContext(WareHouseContext);

    return (

        <View style={styles.screen}>

            <HeaderBg style={{ height: '100%' }}>

                <View style={styles.dateContainer}>
                    <Text>Date : </Text>
                    <Text style={styles.dateText}>{currentDate}</Text>
                </View>

                <View style={styles.content}>
                   

                        
                            <Input
                                style={styles.input}
                                value={'Staff Code: ' + staffId}
                                editable={false} />



                            <Input
                                style={styles.input}
                                value={'Name: ' + staffName}
                                editable={false} />

                           
                        </View>
                 
                
                        <View style={styles.wareHouseContainer}>
                            <Text>Select Warehouse : </Text>
                            <View>

                                <FlatList
                                    horizontal={true}
                                    data={WAREHOUSE}
                                    keyExtractor={item => item.id}
                                    renderItem={itemData => 
                                       
                                        <WareHouseItem
                                            image={itemData.item.image}
                                            title={itemData.item.title}
                                            onWareHouseScreen={(item) => {
                                                warehouse.name =  itemData.item.title
                                                props.navigation.navigate(
                                                    'WareHouse',
                                                    {
                                                        
                                                        warehouseTitle: itemData.item.title
                                                    }
                                                );
                                            }}

                                        />

                                        
                                    }
                                />
                            </View>
                            </View>
            </HeaderBg>
        </View>
    );
};


const styles = StyleSheet.create({
    screen: {
    },
    content: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    wareHouseContainer:{
      paddingHorizontal:45
    },
    dateContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-end',
        alignSelf: 'flex-end',
        paddingEnd: 50
    },
    dateText: {
        color: 'white'
    }
})

SelectLocationScreen.navigationOptions ={
    title: 'Select Location',
};

export default SelectLocationScreen;