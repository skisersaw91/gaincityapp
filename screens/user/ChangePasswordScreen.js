import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import Input from '../../components/Input';
import HeaderBg from '../../components/HeaderBg';
import CustomButton from '../../components/CustomButton';

const ChangePasswordScreen = props => {
    return (
        <View style={styles.screen}>
            <HeaderBg style={{ height: '100%' }}>
                <View style={styles.content}>
                    <Input style={styles.input} placeholder="Current Password" />
                    <Input style={styles.input} placeholder="New Password" />
                    <CustomButton onPress={() => { }}>UPDATE</CustomButton>
                </View>
            </HeaderBg>

        </View>
    );
};

const styles = StyleSheet.create({
    screen:{
    },
    content:{
        flex:0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
})

ChangePasswordScreen.navigationOptions ={
    title: 'Change Password',
};

export default ChangePasswordScreen;