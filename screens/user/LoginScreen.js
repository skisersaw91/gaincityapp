import React, { useState } from 'react';
import { View,  Keyboard,Alert,Text, Button, StyleSheet } from 'react-native';
import Input from '../../components/Input';
import STAFF from '../../data/user-dummy-data';
import HeaderBg from '../../components/HeaderBg';
import CustomButton from '../../components/CustomButton';

const LoginScreen = props => {

    const [staffCode, setStaffCode] = useState('');
    const [password, setPassword] = useState('');

    const staffCodeInputHandler = inputText => {
        setStaffCode(inputText);
    };

    const passwordInputHandler = inputText => {
        setPassword(inputText);
    };

    const loginAuthenticationHandler = () => {

        if(staffCode !== STAFF.staff_id || password !== STAFF.password){
            Alert.alert(
                'Invalid Staff ID / Password!',
                'Please enter correct Staff ID / Password!',
                [{ text: 'Okay', style: 'destructive'}]
            );
            return;

        }else{
            props.navigation.navigate('SelectLocation', { 
                staffId: STAFF.staff_id,
                staffName: STAFF.name
            });

            setStaffCode('');
            setPassword('');
        }

    }

    return (
        <View style={styles.screen}>
            <HeaderBg style={{ height: '100%' }}>
                <View style={styles.content}>
                    
                    <Input
                        style={styles.input}
                        autoCapitalize="none"
                        autoCorrect={false}
                        placeholder="Staff Code"
                        onChangeText={staffCodeInputHandler}
                        value={staffCode}
                    />

                    <Input
                        style={styles.input}
                        autoCapitalize="none"
                        autoCorrect={false}
                        placeholder="Password"
                        onChangeText={passwordInputHandler}
                        value={password}
                    />

                    <CustomButton onPress={loginAuthenticationHandler}>LOGIN</CustomButton>
                    <Text 
                    style={styles.text}
                    onPress={() => {
                        props.navigation.navigate('ChangePassword', {

                        });
                    }}>Change password?</Text>
                </View>
            </HeaderBg>
        </View>
    );
};

const styles = StyleSheet.create({
    screen:{
    },
    content:{
        flex:0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        padding: 10
    },
    inputContainer:{
       
        alignItems:'center',
        justifyContent: 'center',
    },
})

export default LoginScreen;