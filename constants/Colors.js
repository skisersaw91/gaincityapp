export default{
    primary: '#ffa500',
    accent: '#ffd000',
    black: '#022834',
    red: '#Db4d28',
    green: '#28db93',
    white: '#ffffff'
}