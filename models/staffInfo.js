class StaffInfo{
    constructor(staff_id, password, name){
        this.staff_id = staff_id;
        this.password = password;
        this.name = name;
    }
}

export default StaffInfo;