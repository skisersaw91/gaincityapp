class Item {
    constructor(id, title, image, bgColor){
        this.id = id;
        this.title = title;
        this.image = image;
        this.bgColor = bgColor;
    }
}

export default Item;