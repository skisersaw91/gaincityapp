import React, {useState} from 'react';

const WareHouseContext = React.createContext();

const WareHouseProvider = (props) => {
   
const [name, setName] = useState('');
    
        return (
            <WareHouseContext.Provider
            value={{name: name}}
            >
                {props.children}
            </WareHouseContext.Provider>
        );
    }


export {WareHouseProvider, WareHouseContext}