import React from 'react';
import { View,Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';

const CustomButton = props => {

    return (
        <TouchableOpacity activeOpacity={0.6} onPress={props.onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>{props.children}</Text>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({

    button:{
        width:310,
        backgroundColor: Colors.white,
        paddingVertical: 12,
        paddingHorizontal: 30,
        borderRadius: 25

    },
    buttonText:{
        textAlign:'center',
        color:'black',
        fontSize: 18
    }
});

export default CustomButton;