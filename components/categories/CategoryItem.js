import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity, TouchableNativeFeedback, Platform, Alert } from 'react-native';


const CategoryItem = props => {
    let TouchableCmp = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableCmp = TouchableNativeFeedback;
    }

    return (
        <View style={styles.touchable}>
            <TouchableCmp onPress={() => { }} useForeground>

                <View style={styles.container}>
                    <View style={styles.item}>
                        <Image resizeMode='contain'
                            style={{ ...styles.image }}
                            source={props.image} />
                    </View>

                </View>

            </TouchableCmp>
        </View>
    );
};

const styles = StyleSheet.create({
    touchable: {
        borderRadius: 10,
        overflow: 'hidden'
    },
    container: {
        paddingVertical: 5,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 130,
        width: 130,
        borderRadius: 10,
        overflow: 'hidden',
    },
    image: {
        width: '100%',
        height: undefined,
        aspectRatio: 1,
    }
});

export default CategoryItem;