import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

const Logo = props => {
    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <Image
                    style={styles.image}
                    source={require('../assets/gain_city_logo.png')}
                    resizeMode='contain'
                />
            </View>

            <View>
                <Text style={styles.text}>
                    GAIN CITY
                </Text>
            </View>


        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', 
        justifyContent: 'space-between',
        alignItems:'center'
        
    },
    text: {
        fontSize: 30,
        fontWeight: 'bold'
    },
    imageContainer: {
        padding: 10
    },
    image: {
        width: 50,
        height: 100,
    }
});

export default Logo;