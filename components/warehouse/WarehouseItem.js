import React from 'react';
import { View, Text, Image, Button, StyleSheet, TouchableOpacity, TouchableNativeFeedback, Platform } from 'react-native';

const WareHouseItem = props => {
    let TouchableCmp = TouchableOpacity;

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableCmp = TouchableNativeFeedback;
    }

    return (
        <View style={styles.touchable}>
            <TouchableCmp onPress={props.onWareHouseScreen} useForeground>
                <View style={styles.imageContainer}>
                    <View style={styles.item}>
                        <Image
                            resizeMode='contain'
                            style={styles.image}
                            source={props.image} />
                    </View>
                </View>
            </TouchableCmp>
        </View>
    );
};

const styles = StyleSheet.create({
    touchable: {
        borderRadius: 10,
        overflow: 'hidden'
    },
    imageContainer: {
        paddingEnd: 5,
    },
    item: {
        alignItems:'center',
        justifyContent:'center',
        height: 120,
        width:120,
        borderRadius: 10,
        overflow: 'hidden',
    },
    image: {
        width: '100%',
        height: undefined,
        aspectRatio: 1,

    }
});

export default WareHouseItem;