import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Colors from '../constants/Colors';
import Logo from '../components/Logo';

const HeaderBg = props => {
    return (
        <View style={{...styles.headerScreen, ...props.style}}>

            <View style={styles.headerContentScreen}>
                <Logo />
                <View style={styles.wareHouseTextContainer}>
                    <Text style={styles.wareHouseText}>{props.warehouse}</Text>
                </View>
            </View>
            {props.children}
        </View>
    );
};


const styles = StyleSheet.create({
   
    headerScreen:{
        backgroundColor: Colors.primary,
        paddingVertical:10,
        height:undefined
    },
    headerContentScreen:{
        alignItems: 'center',
        justifyContent: 'center',
    },
     wareHouseTextContainer:{
        
        alignItems: 'center',
        justifyContent: 'center',
    },
    wareHouseText: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white'
    },
})

export default HeaderBg;