import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

const Input = props => {
    return (<TextInput {...props} style = {{...styles.input, ...props.style}}/>);
};

const styles = StyleSheet.create({
    input: {
        width: '80%',
        height: 30,
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        marginVertical: 10,
    }
});

export default Input;