import RecentItem from '../models/recentItems';
import Colors from '../constants/Colors';

const RECENT = [
    new RecentItem('r1','System', require('../assets/system-button.png')),
    new RecentItem('r2','Commercial', require('../assets/commercial-button.png')),
    new RecentItem('r3','Air Curtain', require('../assets/ari-curtain-button.png')),
];

export default RECENT;