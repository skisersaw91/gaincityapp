import Warehouse from "../models/warehouse";


const WAREHOUSE = [
    new Warehouse('wh1','SUNGIE KADUT WAREHOUSE', require('../assets/sk-wh.png')),
    new Warehouse('wh2','AMK WAREHOUSE', require('../assets/amk-wh.png')),
];

export default WAREHOUSE;