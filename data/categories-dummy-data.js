import Item from '../models/items';
import Colors from '../constants/Colors';

const CATEGORIES = [
    new Item('cat1','Accessories', require('../assets/accessories.png'), Colors.black),
    new Item('cat2','Bolt & Nut & Plug', require('../assets/bolt-nut.png'), Colors.red),
    new Item('cat3','Cable Tray', require('../assets/cable-tray.png'), Colors.green),
    new Item('cat4','Cable Tray', require('../assets/cable-tray.png'), Colors.green),
    new Item('cat5','Bolt & Nut & Plug', require('../assets/bolt-nut.png'), Colors.red),
    new Item('cat6','Accessories', require('../assets/accessories.png'), Colors.black),
    new Item('cat7','Accessories', require('../assets/accessories.png'), Colors.black),
    new Item('cat8','Bolt & Nut & Plug', require('../assets/bolt-nut.png'), Colors.red),
    new Item('cat9','Cable Tray', require('../assets/cable-tray.png'), Colors.green),
    new Item('cat10','Cable Tray', require('../assets/cable-tray.png'), Colors.green),
    new Item('cat11','Bolt & Nut & Plug', require('../assets/bolt-nut.png'), Colors.red),
    new Item('cat12','Accessories', require('../assets/accessories.png'), Colors.black),
];

export default CATEGORIES;