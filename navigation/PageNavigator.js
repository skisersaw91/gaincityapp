import React from 'react';
import { Platform,Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';

import Colors from '../constants/Colors';
import { MaterialCommunityIcons,Ionicons } from '@expo/vector-icons';


import ChangePasswordScreen from '../screens/user/ChangePasswordScreen';
import LoginScreen from '../screens/user/LoginScreen';
import SelectLocationScreen from '../screens/user/SelectLocationScreen';
import ScanQrCodeScreen from '../screens/warehouse/ScanQrCodeScreen';
import CategoriesScreen from '../screens/warehouse/CategoriesScreen';


const ScanQrCodeNavigator = createStackNavigator({
    ScanQrCode:{
        screen: ScanQrCodeScreen
    }
})


const CategoriesNavigator = createStackNavigator({
    Categories:{
        screen: CategoriesScreen
    }
})

const tabScreenConfig = {

    ScanQRCode: {
        screen: ScanQrCodeNavigator,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return (<MaterialCommunityIcons name='qrcode-scan' size={25} color={tabInfo.tintColor} />);
            },
            tabBarColor: Colors.primary,
            tabBarLabel: Platform.OS === 'android' ? <Text>Scan QR Code</Text> : 'Scan QR Code'
        }
    },
    Categories: {
        screen: CategoriesNavigator,
        navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return (<Ionicons name='search-sharp' size={25} color={tabInfo.tintColor} />);
            },
            tabBarColor: Colors.accent,
            tabBarLabel: Platform.OS === 'android' ? <Text>Select Item By Category</Text> : 'Select Item By Category'

        }
    }
}



const WareHouseTabNavigator = 
Platform.OS === 'android' 
? createMaterialBottomTabNavigator(tabScreenConfig,{
    activeColor: 'white',
    shifting: true,
    barStyle:{
        backgroundColor: Colors.primary
    }
}) 
:  createBottomTabNavigator(
    tabScreenConfig, {
    tabBarOptions: {
        activeTintColor: Colors.accent
    }
});

WareHouseTabNavigator.navigationOptions= {
    // Hide the header from root stack
    headerShown: false
  };

const MainNavigator = createStackNavigator({
    Login: LoginScreen,
    ChangePassword: ChangePasswordScreen,
    SelectLocation: SelectLocationScreen,
    WareHouse: {
       screen: WareHouseTabNavigator
    }
},{
   
});

export default createAppContainer(MainNavigator);